# TS_PROJECT
练习TS项目
# mock 数据 
https://generatedata.com/zh/generator
# 计划大纲
（1）搭建 websocket 服务端和客户端进行发送消息（完成）

（2）进行后端 ws 的编写(完成)

（3）引入 mock 进行请求数据的模拟

（4）接入 axios 请求库，新建一个 http 请求的服务端

（5）新建一个每周一个 npm 库的测试 demo 

（6）引入 vuex 和 pinia 进行比较，创建一个资源管理的demo

（7）跟随视频进行敲项目 （重点）

（8）开始学习 html 的相关知识
