import _ from 'lodash';
import messageApi from '../api/message.js';

const defaultGetMessageInterval = 10000;
let socket = null
const MessageProvider = {
  name: 'MessageProvider',

  props: {
    clientId: {
      type: String,
      default: ''
    },
    token: {
      type: String,
      default: ''
    },
    businessCodes: {
      type: Array,
      default: () => []
    },
    beforeRead: {
      type: Function,
      default: () => false
    },
    speechDuration: {
      type: Number | null,
      default: null
    }
  },

  provide() {
    const _self = this;
    return {
      messageProvider: _self
    };
  },

  data() {
    this.getTimer = null;
    return {
      messages: [],
      newMessagesIds: [], // 要播放的消息id
      newMessages: [], // 要播放的消息
      getError: null,
      getInterval: defaultGetMessageInterval,
      failureTimes: 0,
      retryTimer: null,
      // socket链接状态
      socketStatus: false,
      // speech: new SpeechSynthesisUtterance()
      // 循环播放的定时器
      loopPlayMessagesTimer: null,
      // 循环播放的信息
      loopPlayMessages: [],
    };
  },

  watch: {
    businessCodes() {
      this.pullMessages();
    }
  },

  created() {
    this.clearExpiredMessages();
    // 初始化 MsunWebSocket
    this.initSocket();
  },

  beforeDestroy() {
    this.clearGetTimer();
    this.clearSocket();
    this.clearLoopPlayMessagesTimer();
  },

  methods: {
    // 初始化socket
    initSocket() {
      if (!this.clientId || !this.token) {
        throw new Error('缺少必须的参数 clientId 或 token，但是message组件需要它');
      }
      // 查询
      const hostname = location.hostname === 'localhost' ? 'thirdpart-graytest.msunhis.com:9443' : `${location.hostname}${location.port === '' ? '' : `:${location.port}`}`
			const socketScheme = `${location.protocol === 'http' && location.hostname !== 'localhost' ? 'ws' : 'wss'}`
      const socketHost = `${socketScheme}://${hostname}`;
      const socketPath = '/msun-websocket-server/ws-server';
      const socketParams = {
        // 用户Id
        clientId: this.clientId,
        appName: 'system-message-websocket',
        busiName: 'messageSendSocket',
        token: this.token,
        version: '1.0',
        host: location.hostname === 'localhost' ? 'thirdpart-graytest.msunhis.com' : location.hostname,
        storage: false,
        env: this.getEnv()
      }
      this.socket = new MsunWebSocket(
        `${socketHost}${socketPath}?${Object.entries(socketParams).map(item => `${item[0]}=${item[1]}`).join('&')}`,
        {
          adapter: 'merge'
        }
      );
      this.socket.addEventListener('open', this.socketOpen);
      this.socket.addEventListener('close', this.socketClose);
      this.socket.addEventListener('message', this.socketMessage)
      this.socket.addEventListener('error', this.socketError)
    },
    socketOpen() {
      this.socketStatus = true;
      // 连接成功拉取一次消息
      this.refreshMessages();
      console.warn('消息组件socket连接成功')
    },
    socketMessage(event) {
      const data = JSON.parse(event.data || '{}');
      const content = JSON.parse(data.content || '{}');
      if (content === 'Y') {
        // 每次收到消息就拉取一下列表
        this.getMessageList('core-message-websocket');
      }
    },
    socketError() {
      // 测试环境每隔1分钟会端口socket链接，线上是10分钟，所以在服务器主动断开链接的时候我们要尝试重新链接
      // 如果没有在检测就创建一个倒计时
      this.socketReconnect();
    },
    socketClose() {
      this.socketReconnect();
    },
    socketReconnect() {
      console.warn('消息组件socket重连')
      if (!this.retryTimer) {
        this.retryTimer = setTimeout(() => {
          this.failureTimes = 0
          this.retryTimer = null;
        }, 1000 * 60);
      }
      if (this.failureTimes < 3) {
        this.failureTimes += 1;
        this.initSocket();
      }
      // 如果在1分钟内重复失败3次，则降级为主动轮询
      if (this.failureTimes >= 3) {
        console.warn('消息组件socket短期重试次数过多，降级为轮询')
        this.pullMessages();
        return;
      }
    },
    // 清除socket的事件注册
    clearSocket() {
			if (this.socket) {
				this.socket.removeEventListener('open', this.socketOpen);
				this.socket.removeEventListener('message', this.socketMessage);
				this.socket.removeEventListener('error', this.socketError);
			}
    },
    // 获取本地存储的消息
    getLocalStorageMessages() {
      let localBroadcastedMessages = localStorage.getItem('msunlibui-broadcastedMessages')
      localBroadcastedMessages = localBroadcastedMessages ? JSON.parse(localBroadcastedMessages) : {}
      return localBroadcastedMessages
    },
    // 清空本地存储中过期的消息
    clearExpiredMessages() {
      const localBroadcastedMessages = this.getLocalStorageMessages()
      const msgIds = Object.keys(localBroadcastedMessages)
      const currTime = new Date().getTime()
      msgIds.forEach(id => {
        if (currTime - parseInt(localBroadcastedMessages[id], 10) > 30 * 24 * 60 * 60 * 1000) {
          delete localBroadcastedMessages[id]
        }
      })

      localStorage.setItem('msunlibui-broadcastedMessages', JSON.stringify(localBroadcastedMessages))
    },
    pullMessages() {
      this.clearGetTimer();
      this.getMessageList("core-message-downgrade-polling")
        .finally(() => {
          this.getTimer = setTimeout(this.pullMessages, this.getInterval);
        });
    },

    refreshMessages() {
      this.getMessageList('core-message-refresh');
    },

    clearGetTimer() {
      if (this.getTimer) {
        clearTimeout(this.getTimer);
        this.getTimer = null;
      }
    },

    // 清除循环播放信息定时器
    clearLoopPlayMessagesTimer() {
      if(this.loopPlayMessagesTimer) {
        clearTimeout(this.loopPlayMessagesTimer)
        this.loopPlayMessagesTimer = null
      }
    },
    // 通过接口获取消息
    getMessageList(source) {
      const params = {
        businessCodes: this.businessCodes.join(','),
        source,
      };
      return messageApi
        .fetchMessageList(params)
        .then(_list => {
          // fixme: unreadedMessageList api bug
          const list = _list.slice(0, 300);
          this.messages = list.map(item => {
            return {
              ...item,
              msgExtInfo: item.msgExt ? JSON.parse(item.msgExt) : null
            };
          });
          this.getError = null;
        })
        .catch(err => {
          this.getError = err;
        })
        .finally(this.emitEvents);
    },

    emitEvents() {
      console.log('进入选择如何播报')
      if (this.getError) {
        this.$emit('error', this.getError);
        return;
      }
      const msgs = _.cloneDeep(this.messages);

      for (let newMsg of msgs) {
        if (!this.newMessagesIds.includes(newMsg['msgId'])) {
          this.newMessagesIds.push(newMsg['msgId']);
          this.newMessages.push(newMsg);
        }
      }

      const localBroadcastedMessages = this.getLocalStorageMessages()

      if (this.newMessages.length > 0) {
        for (let msg of this.newMessages) {
          if (!localBroadcastedMessages[msg.msgId]) {
            // 先将要播报的消息缓存到本地
            const localBroadcastedMessages = this.getLocalStorageMessages()
            localBroadcastedMessages[msg.msgId] = new Date().getTime().toString()
            localStorage.setItem('msunlibui-broadcastedMessages', JSON.stringify(localBroadcastedMessages))
            // 从本地存储判断，只有未播放过的消息，才可以播放
            console.log('进入首次播报');
            this.playMessage(msg);
          }
        }
      }
      // 定时器未开启并且开启循环播放配置，执行循环播放的逻辑
      if(!this.loopPlayMessagesTimer && this.speechDuration !== null){
        this.playLoopMessage()
      }
      this.$emit('messages', _.cloneDeep(this.messages));
    },
    // 语音播报消息
    playMessage(msg) {
      const hookState = this.beforeRead(msg)
      if (hookState) {
        console.log(11111);
        let speech = new SpeechSynthesisUtterance(msg['msgContent']);
        speechSynthesis.speak(speech);
      }
    },
    // 循环消息播报
    playLoopMessage(){
      console.log('进入循环播放之前',this.loopPlayMessagesTimer);
      // 获取最新的消息
      this.loopPlayMessages = _.cloneDeep(this.messages);
      if(this.loopPlayMessages.length > 0){
        for(let msg of this.loopPlayMessages){
          console.log('进入循环播放信息',this.loopPlayMessages);
          this.playMessage(msg)
        }
      }
      // 开启定时器
      this.loopPlayMessagesTimer = setTimeout(()=>{
        console.log('进入定时器，this.messages',this.messages);
        if(this.messages.length > 0){
          console.log('再一次循环');
          this.playLoopMessage()
        }else{
          // 清除定时器
          this.clearLoopPlayMessagesTimer()
        } 
      },this.speechDuration)
    },
    /**
     * 获取当前环境的标准值
     * @returns {'dev'|'gray'|'test'|'pro'}
     */
    getEnv() {
      if ( location.hostname === 'localhost') return 'dev'
      if ( location.hostname.includes('graytest')) return 'gray'
      if ( ['msunsoft-test.msunhis.com', 'kunpeng.msunhis.com'].includes(location.hostname)) return 'test'
      return 'pro'
    }
  },

  render() {
    return this.$slots.default ? this.$slots.default[0] : null;
  }
};

export default MessageProvider;
