import * as WebSocket from 'ws';  
  
// 创建WebSocket服务器实例  
const wss = new WebSocket.Server({ port: 3080 });  
console.log('进入ws');

// 监听连接事件  
wss.on('connection', (ws) => {  
  console.log('Client connected');  
  ws.send(1);
  // 接收客户端消息事件  
  ws.on('message', (message) => {  
    const strMessage = message.toString();  
    console.log(`Received message: ${strMessage}`);  
    if (strMessage === 'yes') {  
      ws.send('success');  
    } 
  });  
    
  // 监听关闭连接事件  
  ws.on('close', () => {  
    console.log('Client disconnected');  
  });
  
  //监听错误
  ws.on('error', () => {
    console.log('onerror')
  })
});